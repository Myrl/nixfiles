# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  networking = {
    extraHosts = "192.168.2.13 lappy";
    hostName = "myrl"; # Define your hostname.
  };

  programs.ssh = {
    extraConfig = ''
      IdentityFile ~/.ssh/%h_rsa
    '';
  };

  services.xserver = {
    enable = true;
    videoDrivers = ["intel"];
    desktopManager.kde5.enable = true;
    windowManager.xmonad.enable = true;
    windowManager.xmonad.enableContribAndExtras = true;
    displayManager.sddm.enable = true;
    displayManager.sessionCommands = ''
      compton -b
      [[ -f .fehbg ]] && bash .fehbg
    '';
  };

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda";

  boot.kernel.sysctl = {
    "vm.swappiness" = 0;
  };

  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Asia/Manila";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  # environment.systemPackages = with pkgs; [
  #   wget
  # ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.kdm.enable = true;
  # services.xserver.desktopManager.kde4.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.myrl = {
    isNormalUser = true;
    home = "/home/myrl";
    description = "Myrl Hex";
    extraGroups = ["wheel" "video" "audio"];
    shell = "/run/current-system/sw/bin/zsh";
  };
  

  programs.zsh = {
    enable = true;
  };

  environment.systemPackages = with pkgs; let 
      myHaskellEnv = haskellPackages.ghcWithPackages
        (haskellPackages: with haskellPackages; [
          arithmoi
          monad-loops
	  hlint
	  hdevtools
          parsec
          attoparsec
          network
	  xmonad
        ]); 
    in 
      [
	glib
	json_glib
	zlib
	unzip
	zip
        myHaskellEnv
	zathura
	ghc
	jdk
        gcc
        gnumake
        xsel
        vimb
        scrot
        nmap
        mplayer
        htop
        nix-repl
        compton
        wget
        dmenu
        firefox
        neovim
        feh
        git
	mypaint
	gimp
        transmission
	xorg.xwininfo
	qrencode
	chromium
        ruby
        fontforge
        electrum
        sshfsFuse
    ];

  fonts = {
    enableFontDir = true;
    fonts = with pkgs; [
      corefonts
      terminus_font
      hasklig
      tewi-font
      fira-code
      monoid-font
      profont
      dina-font
      gohufont
      unifont
    ];
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "15.09";

  nix.extraOptions = "binary-caches-parallel-connections = 5";

  nixpkgs.config = {
    allowUnfree = true;

    chromium = {
       enablePepperFlash = true;
    };
 
    };
  nix.nixPath = [ "/etc/nixos" "nixos-config=/etc/nixos/configuration.nix" ]; 
}
